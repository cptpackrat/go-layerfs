# go-layerfs
An implementation of fs.FS that presents a stack of filesystem layers as a combined overlay.

# Usage
```go
package main

import (
  "gitlab.com/cptpackrat/go-layerfs"
)

func main () {
  lfs := layerfs.FS{
    os.DirFS("path/to/upper"),
    os.DirFS("path/to/middle"),
    os.DirFS("path/to/lower")}

  if file, err := lfs.ReadFile("some/file"); err == nil {
    // file was opened and read from the first layer it was found in
  } else if errors.Is(err, fs.ErrNotExist) {
    // file was not found in any layer
  }

  if ents, err := lfs.ReadDir("some/dir"); err == nil {
    // dir entries were collected and merged across all layers it was found in
  } else if errors.Is(err, fs.ErrNotExist) {
    // dir was not found in any layer
  }
}
```