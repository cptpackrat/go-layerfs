package layerfs

import (
	"errors"
	"io/fs"
	"sort"
)

// An implementation of fs.FS that presents a stack of filesystem layers as a combined overlay.
type FS []fs.FS

// Open opens the named file for reading and returns it as an fs.File.
// Returns the result of the first layer within the stack to return an
// error other than fs.ErrNotExist.
func (layers FS) Open(name string) (fs.File, error) {
	for _, layer := range layers {
		if file, err := layer.Open(name); !errors.Is(err, fs.ErrNotExist) {
			return file, err
		}
	}
	return nil, fs.ErrNotExist
}

// ReadDir reads and returns the entire named directory. Returns the result
// of calling fs.ReadDir for all layers in the stack and then merging the
// results, giving priority to earlier layers whenever path collisions are
// encountered.
//
// (implements fs.ReadDirFS)
func (layers FS) ReadDir(name string) ([]fs.DirEntry, error) {
	set := map[string]fs.DirEntry{}
	hit := false
	for _, layer := range layers {
		if ents, err := fs.ReadDir(layer, name); err == nil {
			hit = true
			for _, ent := range ents {
				if _, has := set[ent.Name()]; !has {
					set[ent.Name()] = ent
				}
			}
		} else if !errors.Is(err, fs.ErrNotExist) {
			return nil, err
		}
	}
	if !hit {
		return nil, fs.ErrNotExist
	}
	ents := make([]fs.DirEntry, len(set))[:0]
	for _, ent := range set {
		ents = append(ents, ent)
	}
	sort.Slice(ents, func(a, b int) bool {
		return ents[a].Name() < ents[b].Name()
	})
	return ents, nil
}

// ReadFile reads and returns the content of the named file. Returns the
// result of the first layer within the stack to return an error other
// than fs.ErrNotExist when passed to fs.ReadFile.
//
// (implements fs.ReadFileFS)
func (layers FS) ReadFile(name string) ([]byte, error) {
	for _, layer := range layers {
		if file, err := fs.ReadFile(layer, name); !errors.Is(err, fs.ErrNotExist) {
			return file, err
		}
	}
	return nil, fs.ErrNotExist
}

// Stat returns a FileInfo describing the file. Returns the result of
// the first layer within the stack to return an error other than
// fs.ErrNotExist when passed to fs.Stat.
//
// (implements fs.StatFS)
func (layers FS) Stat(name string) (fs.FileInfo, error) {
	for _, layer := range layers {
		if info, err := fs.Stat(layer, name); !errors.Is(err, fs.ErrNotExist) {
			return info, err
		}
	}
	return nil, fs.ErrNotExist
}
