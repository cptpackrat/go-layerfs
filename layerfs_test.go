package layerfs

import (
	"io"
	"io/fs"
	"path"
	"testing"

	"github.com/psanford/memfs"
	"github.com/stretchr/testify/assert"
)

func TestOpen(t *testing.T) {
	testReadFile(t, func(lfs FS, name string) (string, error) {
		if file, err := lfs.Open(name); err != nil {
			return "", err
		} else if data, err := io.ReadAll(file); err != nil {
			return "", err
		} else {
			return string(data), nil
		}
	})
}

func TestReadDir(t *testing.T) {
	lfs := FS{
		buildMemFS(memfs.New(), "", []any{
			memFile{"foo", 0644, "foo layer 1"},
			memTree{"baz", 0777, []any{}},
		}),
		buildMemFS(memfs.New(), "", []any{
			memFile{"foo", 0644, "foo layer 2"},
			memFile{"bar", 0644, "bar layer 2"},
			memFile{"boo", 0644, "boo layer 2"},
			memTree{"baz", 0777, []any{
				memFile{"qux", 0644, "qux layer 2"},
			}},
		}),
		buildMemFS(memfs.New(), "", []any{
			memTree{"baz", 0777, []any{
				memFile{"qux", 0644, "qux layer 3"},
				memFile{"woo", 0644, "woo layer 3"},
			}},
		}),
	}
	type ent struct {
		name string
		dir  bool
	}
	for _, test := range []struct {
		name string
		ents []ent
		err  any
	}{
		{".", []ent{
			{"bar", false},
			{"baz", true},
			{"boo", false},
			{"foo", false},
		}, nil},
		{"baz", []ent{
			{"qux", false},
			{"woo", false},
		}, nil},
		{"foo", []ent{}, "not implemented"},
		{"bad", []ent{}, fs.ErrNotExist},
		{"..", []ent{}, fs.ErrInvalid},
	} {
		ents, err := lfs.ReadDir(test.name)
		switch exp := test.err.(type) {
		case error:
			assert.ErrorIs(t, err, exp)
		case string:
			assert.ErrorContains(t, err, exp)
		default:
			assert.NoError(t, err)
			for idx, ent := range ents {
				assert.Equal(t, test.ents[idx].name, ent.Name())
				assert.Equal(t, test.ents[idx].dir, ent.IsDir())
			}
		}
	}
}

func TestReadFile(t *testing.T) {
	testReadFile(t, func(lfs FS, name string) (string, error) {
		if data, err := lfs.ReadFile(name); err != nil {
			return "", err
		} else {
			return string(data), nil
		}
	})
}

func testReadFile(t *testing.T, readFile func(lfs FS, name string) (string, error)) {
	lfs := FS{
		buildMemFS(memfs.New(), "", []any{
			memFile{"foo", 0644, "foo layer 1"},
			memTree{"baz", 0777, []any{}},
		}),
		buildMemFS(memfs.New(), "", []any{
			memFile{"foo", 0644, "foo layer 2"},
			memFile{"bar", 0644, "bar layer 2"},
			memFile{"boo", 0644, "boo layer 2"},
			memTree{"baz", 0777, []any{
				memFile{"qux", 0644, "qux layer 2"},
			}},
		}),
		buildMemFS(memfs.New(), "", []any{
			memTree{"baz", 0777, []any{
				memFile{"qux", 0644, "qux layer 3"},
				memFile{"woo", 0644, "woo layer 3"},
			}},
		}),
	}
	for _, test := range []struct {
		name, data string
		err        any
	}{
		{"foo", "foo layer 1", nil},
		{"bar", "bar layer 2", nil},
		{"boo", "boo layer 2", nil},
		{"baz", "", "is a directory"},
		{"baz/qux", "qux layer 2", nil},
		{"baz/woo", "woo layer 3", nil},
		{"baz/bad", "", fs.ErrNotExist},
		{"baz/..", "", fs.ErrInvalid},
		{"bad", "", fs.ErrNotExist},
		{"..", "", fs.ErrInvalid},
	} {
		data, err := readFile(lfs, test.name)
		switch exp := test.err.(type) {
		case error:
			assert.ErrorIs(t, err, exp)
		case string:
			assert.ErrorContains(t, err, exp)
		default:
			assert.NoError(t, err)
			assert.Equal(t, test.data, data)
		}
	}
}

func TestStat(t *testing.T) {
	lfs := FS{
		buildMemFS(memfs.New(), "", []any{
			memFile{"foo", 0644, "foo layer 1"},
			memTree{"baz", 0777, []any{}},
		}),
		buildMemFS(memfs.New(), "", []any{
			memFile{"foo", 0644, "foo layer 2"},
			memFile{"bar", 0644, "bar layer 2"},
			memFile{"boo", 0644, "boo layer 2"},
			memTree{"baz", 0777, []any{
				memFile{"qux", 0644, "qux layer 2"},
			}},
		}),
		buildMemFS(memfs.New(), "", []any{
			memTree{"baz", 0777, []any{
				memFile{"qux", 0644, "qux layer 3"},
				memFile{"woo", 0644, "woo layer 3"},
			}},
		}),
	}
	for _, test := range []struct {
		name string
		mode fs.FileMode
		size int64
		dir  bool
		err  any
	}{
		{"foo", 0644, 11, false, nil},
		{"bar", 0644, 11, false, nil},
		{"boo", 0644, 11, false, nil},
		{"baz", 0777, 0, true, nil},
		{"baz/qux", 0644, 11, false, nil},
		{"baz/woo", 0644, 11, false, nil},
		{"baz/bad", 0, 0, false, fs.ErrNotExist},
		{"baz/..", 0, 0, false, fs.ErrInvalid},
		{"bad", 0, 0, false, fs.ErrNotExist},
		{"..", 0, 0, false, fs.ErrInvalid},
	} {
		info, err := lfs.Stat(test.name)
		switch exp := test.err.(type) {
		case error:
			assert.ErrorIs(t, err, exp)
		case string:
			assert.ErrorContains(t, err, exp)
		default:
			assert.NoError(t, err)
			assert.Equal(t, path.Base(test.name), info.Name())
			assert.Equal(t, test.dir, info.IsDir())
			switch test.dir {
			case true:
				assert.Equal(t, test.mode|fs.ModeDir, info.Mode())
			case false:
				assert.Equal(t, test.mode, info.Mode())
				assert.Equal(t, test.size, info.Size())
			}
		}
	}
}

type memTree struct {
	name string
	mode fs.FileMode
	data []interface{}
}

type memFile struct {
	name string
	mode fs.FileMode
	data string
}

func buildMemFS(dest *memfs.FS, base string, nodes []interface{}) *memfs.FS {
	for _, node := range nodes {
		switch node := node.(type) {
		case memTree:
			name := path.Join(base, node.name)
			dest.MkdirAll(name, node.mode)
			buildMemFS(dest, name, node.data)
		case memFile:
			name := path.Join(base, node.name)
			dest.WriteFile(name, []byte(node.data), node.mode)
		default:
			panic("bad tree")
		}
	}
	return dest
}
